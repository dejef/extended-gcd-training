# Jeffrey De La Mare

from ctypes import cdll

print("A good example: http://www-math.ucdenver.edu/~wcherowi/courses/m5410/exeucalg.html")

lib = cdll.LoadLibrary("target/release/librsa_problem_gen.dylib")

a = input("First value: ")
b = input("Second value: ")

# TODO confirm they're numbers
a = int(a)
b = int(b)
print()

lib.extended_gcd(a, b)
