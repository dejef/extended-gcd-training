//use num::{Integer, NumCast, Zero};
//use std::fmt::{Debug, Display};
//use std::str::FromStr;
//use std::cmp::PartialEq;
//use std::ops::{Sub, Div, Mul};
use std::io::{self};

/// Euclidean Algorithm for finding the greatest common denominator of 
/// two integers.
/// 
/// # Examples
/// 
/// ```
/// use util::math;
/// let mut a: i64 = 90; let mut b: i64 = 12;
/// let gcd = math::gcd(a, b);
/// 
/// assert_eq!(gcd, 6);
/// ```
#[no_mangle]
pub extern fn gcd(a: i64, b: i64) -> i64 
{   
    if a == 0 { 
        return b;
    }   
    gcd(b % a, a)  
}

/// Extended Euclidean Algorithm for finding the coefficients of Bezout's 
/// identity as well as the gcd.
/// 
/// # Examples
/// 
/// ```
/// use util::math;
/// let mut a: i64 = 1398; let mut b: i64 = 324;
/// let tup = math::extended_gcd(a, b);
/// 
/// assert_eq!((-19, 82, 6, -233, 54), tup);
/// ```
#[no_mangle]
pub extern fn extended_gcd(a: i64, b: i64) 
{
    let mut coprime = false;

    // (Bezout coefficient old_s & old_t, gcd, quotients by gcd t & s)
    let (old_s, old_t, old_r, t, s) = extended_gcd_helper(a, b);
    if gcd(a, b) == 1 {
        coprime = true;
    }
    println!("If coprime, x is the multiplicative inverse a modulo b \n\
              and y is the multiplicative inverse b modulo a.");
    println!("Coprime: {}", coprime);
    println!("Bezout coefficients: x: {} y: {}", old_s, old_t);
    println!("Greatest common divisor: {}", old_r);
    println!("Quotients by the gcd: {} {}", t, s);
}


fn extended_gcd_helper(a: i64, b: i64) -> (i64, i64, i64, i64, i64)
{   
    // http://www.math.cmu.edu/~bkell/21110-2010s/extended-euclidean.html
    // https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
    // iterative implementation
    let mut s = 0; let mut old_s = 1;
    let mut t = 1; let mut old_t = 0;
    let mut r = b; let mut old_r = a;

    while r != 0 {
        let quotient = old_r / r;

        println!("{} = {}({}) + {}", old_r, quotient, r, old_r % r);

        let prov = r;
        r = old_r - quotient * prov;
        old_r = prov;
        println!("{} {} {}", prov, r, old_r);
        let prov = s;
        s = old_s - quotient * prov;
        old_s = prov;
        println!("{} {} {}", prov, s, old_s);

        let prov = t;
        t = old_t - quotient * prov;
        old_t = prov;
        println!("{} {} {}", prov, t, old_t);
        
        let mut buffer = String::new();
        match io::stdin().read_line(&mut buffer) {
            Ok(_) => (),
            Err(error) => println!("error: {}", error)
        }
    }   

    // (Bezout coefficient old_s & old_t, gcd, quotients by gcd t & s)
    (old_s, old_t, old_r, t, s)
} 
