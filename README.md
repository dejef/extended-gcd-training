# Extended Eucidean Algorithm Practice
As said above, this program calculates EGCD and it's fun to play along.
There's not too much to it, for a brush up there is a URL to follow that I've
always liked. I had this in Rust lying about, turns out polymorphism will always
mangle functions in a binary's symbolic table. Makes sense I suppose.

# TODO
* uUhuuhHh obviously need to figure out back substitution.
* Sweet rng with a little bit of guaranteed primes.

# How to use
python3 src/embed.py
